import 'package:e_commerce/core/domain/models/product_model.dart';
import 'package:e_commerce/infrastructure/data_models/product_data_model.dart';
import 'package:e_commerce/infrastructure/model_mappers/product_model_mapper.dart';
import 'package:e_commerce/infrastructure/data_sources/product.datasource.dart';

abstract interface class IProductRepository {
  Future<List<Product>> listProducts();
  Future<Product?> getProductById(String id);
}

class ProductRepository implements IProductRepository {
  final dataSource = LocalStorageDataSource();
  final productModelMapper = ProductModelMapper();

  @override
  Future<Product?> getProductById(String id) async {
    final ProductDataModel? result = await dataSource.getProductById(id);

    if (result != null) {
      final productById = productModelMapper.fromDataModel(result);
      return productById;
    }

    return null;
  }

  @override
  Future<List<Product>> listProducts() async {
    final List<ProductDataModel> dataModels = await dataSource.listProducts();

    final List<Product> productsList =
        dataModels.map((productDataModel) => productModelMapper.fromDataModel(productDataModel)).toList();

    return productsList;
  }
}
