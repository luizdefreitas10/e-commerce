import 'package:e_commerce/infrastructure/data_models/product_data_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:collection/collection.dart';

abstract interface class ILocalStorageDataSource {
  Future<List<ProductDataModel>> listProducts();
  Future<ProductDataModel?> getProductById(String id);
}

class LocalStorageDataSource implements ILocalStorageDataSource {
  static const String _productsCollectionKey = 'PRODUCTS_COLLECTION';

  SharedPreferences? _sharedPreferences;

  @override
  Future<ProductDataModel?> getProductById(String id) async {
    final List<ProductDataModel> result = await listProducts();
    final ProductDataModel? productById = result.firstWhereOrNull((product) => product.id == id);

    if (productById == null) {
      return null;
    }

    return productById;
  }

  @override
  Future<List<ProductDataModel>> listProducts() async {
    final sharedPreferences = await _initializeSharedPreferences;

    // if (!sharedPreferences.containsKey(_productsCollectionKey)) {
    // }
    await sharedPreferences.setStringList(_productsCollectionKey, _toJsonList(productsFixtureList));

    final jsonProductsList = sharedPreferences.getStringList(_productsCollectionKey);

    return jsonProductsList?.map.call((product) => ProductDataModel.fromJson(product)).toList() ?? [];
  }

  Future<SharedPreferences> get _initializeSharedPreferences async {
    if (_sharedPreferences != null) {
      return _sharedPreferences!;
    }

    _sharedPreferences = await SharedPreferences.getInstance();

    return _sharedPreferences!;
  }

  List<ProductDataModel> get productsFixtureList {
    return [
      ProductDataModel(
          id: '01',
          name: 'Samsung Galaxy S23 + Smartphone',
          description: 'Aqui vai a descricao do produto ...',
          price: '6.000,00',
          imageUrl: 'https://m.media-amazon.com/images/I/61vw2GhQ7HL.__AC_SY300_SX300_QL70_ML2_.jpg'),
      ProductDataModel(
          id: '02',
          name: 'Iphone 14 Pro Max',
          description: 'Aqui vai a descricao do produto ...',
          price: '6.000,00',
          imageUrl: 'https://imgs.casasbahia.com.br/55054418/1g.jpg?imwidth=1000'),
      ProductDataModel(
          id: '03',
          name: 'TV LED 32" LG 32LT330HBSB',
          description: 'Aqui vai a descricao do produto ...',
          price: '3.000,00',
          imageUrl: 'https://m.media-amazon.com/images/I/61Fk0CABqoL.__AC_SX300_SY300_QL70_ML2_.jpg'),
      ProductDataModel(
          id: '04',
          name: 'MacBook Air',
          description: 'Aqui vai a descricao do produto ...',
          price: '8.000,00',
          imageUrl: 'https://m.media-amazon.com/images/I/41-RhQeujUL.__AC_SY445_SX342_QL70_ML2_.jpg'),
      ProductDataModel(
          id: '05',
          name: 'Notebook Dell',
          description: 'Aqui vai a descricao do produto ...',
          price: '7.000,00',
          imageUrl:
              'https://i.dell.com/is/image/DellContent/content/dam/ss2/product-images/dell-client-products/notebooks/inspiron-notebooks/inspiron-15-3511/media-gallery/in3511nt_cnb_05000ff090_bk-fpr.psd?fmt=png-alpha&pscan=auto&scl=1&hei=402&wid=606&qlt=100,1&resMode=sharp2&size=606,402&chrss=full'),
      ProductDataModel(
          id: '06',
          name: 'Fone de Ouvido JBL',
          description: 'Aqui vai a descricao do produto ...',
          price: '1.000,00',
          imageUrl: 'https://m.media-amazon.com/images/I/61ZDwijKtxL.__AC_SX300_SY300_QL70_ML2_.jpg'),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
      ProductDataModel(
        id: '07',
        name: 'Mouse Logitech G502',
        description: 'Aqui vai a descricao do produto ...',
        price: '1.000,00',
        imageUrl: 'https://m.media-amazon.com/images/I/71g60fx8csL._AC_SY450_.jpg',
      ),
    ];
  }

  List<String> _toJsonList(List<ProductDataModel> dataModelList) {
    return productsFixtureList.map((product) => product.toJson()).toList();
  }
}
