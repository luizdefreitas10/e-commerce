import 'dart:convert';
import 'package:e_commerce/infrastructure/data_models/product_data_model.dart';

class CartItemDataModel {
  CartItemDataModel({
    required this.id,
    required this.product,
    required this.quantity,
    required this.unitPrice,
    required this.totalPrice,
  });

  factory CartItemDataModel.fromJson(String str) => CartItemDataModel.fromMap(json.decode(str));

  factory CartItemDataModel.fromMap(Map<String, dynamic> json) => CartItemDataModel(
      id: json['id'],
      product: ProductDataModel.fromMap(json['product']),
      quantity: json['quantity'],
      unitPrice: json['unitPrice'],
      totalPrice: json['totalPrice']);

  final String id;
  final ProductDataModel product;
  final int quantity;
  final String totalPrice;
  final String unitPrice;

  String toJson() => json.encode(toMap());

  Map<String, dynamic> toMap() => {
        'id': id,
        'product': product.toMap(),
        'quantity': quantity,
        'unitPrice': unitPrice,
        'totalPrice': totalPrice,
      };
}
