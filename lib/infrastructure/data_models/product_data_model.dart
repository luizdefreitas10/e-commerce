import 'dart:convert';

class ProductDataModel {
  ProductDataModel({
    required this.id,
    required this.name,
    required this.description,
    required this.price,
    required this.imageUrl,
  });

  factory ProductDataModel.fromJson(String str) => ProductDataModel.fromMap(json.decode(str));

  factory ProductDataModel.fromMap(Map<String, dynamic> json) => ProductDataModel(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        price: json['price'],
        imageUrl: json['imageUrl'],
      );

  final String description;
  final String id;
  final String imageUrl;
  final String name;
  final String price;

  String toJson() => json.encode(toMap());

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'description': description,
        'price': price,
        'imageUrl': imageUrl,
      };
}
