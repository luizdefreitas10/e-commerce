import 'dart:convert';

import 'package:e_commerce/infrastructure/data_models/cart_item_data_model.dart';

class SalesRecordDataModel {
  SalesRecordDataModel({
    required this.id,
    required this.customerName,
    required this.recordedAt,
    required this.cartItems,
    required this.totalPrice,
  });

  factory SalesRecordDataModel.fromJson(String str) => SalesRecordDataModel.fromMap(json.decode(str));

  factory SalesRecordDataModel.fromMap(Map<String, dynamic> json) => SalesRecordDataModel(
      id: json['id'],
      customerName: json['customerName'],
      recordedAt: json['recordedAt'],
      cartItems: json['cartItems'].map((cartItem) => CartItemDataModel.fromMap(cartItem)),
      totalPrice: json['totalPrice']);

  final List<CartItemDataModel> cartItems;
  final String customerName;
  final String id;
  final String recordedAt;
  final String totalPrice;

  String toJson() => json.encode(toMap());

  Map<String, dynamic> toMap() => {
        'id': id,
        'customerName': customerName,
        'recordedAt': recordedAt,
        'cartItems': cartItems.map((cartItem) => cartItem.toMap()).toList(),
        'totalPrice': totalPrice,
      };
}
