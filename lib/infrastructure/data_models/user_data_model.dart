class UserDataModel {
  UserDataModel({
    required this.id,
    required this.fullName,
    required this.email,
    required this.password,
  });

  final String email;
  final String fullName;
  final String id;
  final String password;
}
