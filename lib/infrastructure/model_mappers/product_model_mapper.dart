import '../../core/domain/models/product_model.dart';
import '../data_models/product_data_model.dart';

class ProductModelMapper {
  Product fromDataModel(ProductDataModel productDataModel) {
    return Product(
      id: productDataModel.id,
      name: productDataModel.name,
      description: productDataModel.description,
      price: productDataModel.price,
      imageUrl: productDataModel.imageUrl,
    );
  }

  ProductDataModel toDataModel(Product product) {
    return ProductDataModel(
      id: product.id,
      name: product.name,
      description: product.description,
      price: product.price,
      imageUrl: product.imageUrl,
    );
  }
}
