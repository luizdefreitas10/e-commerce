import '../../core/domain/models/user_model.dart';
import '../data_models/user_data_model.dart';

class UserModelMapper {
  User fromDataModel(UserDataModel userdataModel) {
    return User(
      id: userdataModel.id,
      fullName: userdataModel.fullName,
      email: userdataModel.email,
      password: userdataModel.password,
    );
  }

  UserDataModel toDataModel(User user) {
    return UserDataModel(
      id: user.id,
      fullName: user.fullName,
      email: user.email,
      password: user.password,
    );
  }
}
