import 'package:e_commerce/core/domain/models/cart_item.dart';
import 'package:e_commerce/infrastructure/data_models/cart_item_data_model.dart';
import 'package:e_commerce/infrastructure/model_mappers/product_model_mapper.dart';

class CartItemModelMapper {
  final ProductModelMapper _productModelMapper = ProductModelMapper();

  CartItem fromDataModel(CartItemDataModel cartItemDataModel) {
    return CartItem(
        id: cartItemDataModel.id,
        product: _productModelMapper.fromDataModel(cartItemDataModel.product),
        quantity: cartItemDataModel.quantity,
        unitPrice: cartItemDataModel.unitPrice,
        totalPrice: cartItemDataModel.totalPrice);
  }

  CartItemDataModel toDataModel(CartItem cartItem) {
    return CartItemDataModel(
        id: cartItem.id,
        product: _productModelMapper.toDataModel(cartItem.product),
        quantity: cartItem.quantity,
        unitPrice: cartItem.unitPrice,
        totalPrice: cartItem.totalPrice);
  }
}
