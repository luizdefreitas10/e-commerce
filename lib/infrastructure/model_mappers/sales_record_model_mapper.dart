import 'package:e_commerce/core/domain/models/sales_record_model.dart';
import 'package:e_commerce/infrastructure/data_models/sales_record_data_model.dart';
import 'package:e_commerce/infrastructure/model_mappers/cart_item_model_mapper.dart';

class SalesRecordModelMapper {
  final CartItemModelMapper _cartItemModelMapper = CartItemModelMapper();

  SalesRecord? fromDataModel(SalesRecordDataModel salesRecordDataModel) {
    return SalesRecord(
        id: salesRecordDataModel.id,
        customerName: salesRecordDataModel.customerName,
        recordedAt: DateTime.parse(salesRecordDataModel.recordedAt),
        cartItems: salesRecordDataModel.cartItems.map((item) => _cartItemModelMapper.fromDataModel(item)).toList(),
        totalPrice: salesRecordDataModel.totalPrice);
  }

  SalesRecordDataModel toDataModel(SalesRecord salesRecord) {
    return SalesRecordDataModel(
        id: salesRecord.id,
        customerName: salesRecord.customerName,
        recordedAt: salesRecord.recordedAt.toIso8601String(),
        cartItems: salesRecord.cartItems.map((cartItem) => _cartItemModelMapper.toDataModel(cartItem)).toList(),
        totalPrice: salesRecord.totalPrice);
  }
}
