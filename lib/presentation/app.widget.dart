import 'package:flutter/material.dart';
import 'package:e_commerce/presentation/pages/authentication/login/login.controller.dart';
import 'package:e_commerce/presentation/pages/authentication/login/login.widget.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) => LoginWidget(
        controller: LoginController(),
      ),
    );
  }
}
