import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class CartController {
  late List<Map<String, dynamic>> cartList = [];

  static const String _cartCollection = 'CART_COLLECTION';

  Future<void> addCartItemsToSharedPreferences() async {
    final prefs = await _sharedPreferences();
    await prefs.setStringList(_cartCollection, jsonEncode(cartList) as List<String>);
  }

  void addToCart(Map<String, dynamic> product) {
    cartList.add(product);
  }

  void clearCart() {
    cartList.clear();
  }

  Future<void> loadCart() async {
    final prefs = await _sharedPreferences();
    List<String>? cartJson = prefs.getStringList(_cartCollection);

    if (cartJson == null) {
      throw Exception('O carrinho esta vazio.');
    }

    List<dynamic> cartListProducts = json.decode(cartJson as String);
    cartList = cartListProducts.cast<Map<String, dynamic>>();
  }

  Future<SharedPreferences> _sharedPreferences() async {
    return await SharedPreferences.getInstance();
  }
}
