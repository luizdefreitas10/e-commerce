import 'package:e_commerce/core/domain/models/product_model.dart';
import 'package:e_commerce/presentation/pages/home/dashboard/dashboard.controller.dart';
import 'package:e_commerce/presentation/pages/home/product_list/product.list.widget.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({
    required this.userName,
    required this.dashboardController,
    super.key,
  });

  final DashboardController dashboardController;
  final String userName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Página de produtos'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SizedBox(
                width: 200,
                height: 20,
                child: Text('Bem-vindo, $userName'),
              ),
            ),
            Expanded(
              child: FutureBuilder<List<Product>>(
                future: dashboardController.listProducts(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    throw snapshot.error as Error;
                  }

                  if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
                    final data = snapshot.data;
                    if (data != null) {
                      return ProductListWidget(
                        products: data,
                      );
                    }
                  }

                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      persistentFooterAlignment: AlignmentDirectional.center,
      persistentFooterButtons: [
        ElevatedButton(
          onPressed: () {
            dashboardController.navigateBack(context);
          },
          child: const Text('Voltar'),
        ),
      ],
    );
  }
}
