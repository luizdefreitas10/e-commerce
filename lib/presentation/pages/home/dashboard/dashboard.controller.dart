import 'package:e_commerce/core/domain/models/product_model.dart';
import 'package:e_commerce/core/domain/use_cases/product/list_produtcs.use_case.dart';
import 'package:e_commerce/core/domain/use_cases/product/retrieve_product_by_id.use_case.dart';
import 'package:e_commerce/presentation/pages/home/product_detail/product.detail.widget.dart';
import 'package:flutter/material.dart';

class DashboardController {
  Future<Product> getProductById(String id) async {
    final retrieveProductByIdUseCase = RetrieveProductByIdUseCase();

    final product = await retrieveProductByIdUseCase.call(id);

    if (product == null) {
      throw StateError('error: product cannot be null');
    }

    return product;
  }

  Future<List<Product>> listProducts() {
    final listProductsUseCase = ListProductUseCase();

    return listProductsUseCase.call();
  }

  void navigateBack(BuildContext context) {
    Navigator.pop(context);
  }

  Future<void> openProductDetailModal(
    BuildContext context,
    String productId,
  ) {
    return getProductById(productId).then(
      (product) {
        showDialog<void>(
          context: context,
          builder: (context) => ProductDetailWidget(product: product),
        );
      },
    );
  }
}
