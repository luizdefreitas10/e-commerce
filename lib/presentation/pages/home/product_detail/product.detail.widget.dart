import 'package:flutter/material.dart';

import '../../../../core/domain/models/product_model.dart';

class ProductDetailWidget extends StatelessWidget {
  const ProductDetailWidget({required this.product, super.key});

  final Product product;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(12),
      title: Text(product.name),
      content: SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(product.imageUrl),
                    fit: BoxFit.fitWidth,
                  ),
                ),
                constraints: const BoxConstraints(
                  maxWidth: 150.0,
                ),
                margin: const EdgeInsets.all(16),
                child: const SizedBox.square(
                  dimension: 150,
                ),
              ),
            ),
            SizedBox.square(
              dimension: 50,
              child: Text(
                'Description: ${product.description}',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            ),
            Divider(
              height: 12,
              color: Theme.of(context).colorScheme.onSurface.withOpacity(.2),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 24),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: SizedBox.fromSize(
                size: const Size(double.infinity, 50),
                child: Text('Price: R\$${product.price}'),
              ),
            ),
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: const Text('Fechar'),
        ),
      ],
    );
  }
}
