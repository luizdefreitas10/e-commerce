import 'package:e_commerce/core/domain/models/product_model.dart';
import 'package:e_commerce/presentation/pages/home/dashboard/dashboard.controller.dart';
import 'package:flutter/material.dart';

class ProductListWidget extends StatelessWidget {
  ProductListWidget({required this.products, super.key});

  final DashboardController dashboardController = DashboardController();
  final List<Product> products;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const Divider(),
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 10,
      ),
      itemCount: products.length,
      itemBuilder: (context, index) {
        final product = products[index];
        return ListTile(
          onTap: () {
            dashboardController.openProductDetailModal(context, product.id);
          },
          trailing: Text('R\$${product.price}'),
          leading: Image.network(
            product.imageUrl,
            width: 50,
            fit: BoxFit.cover,
          ),
          title: Text(product.name),
          subtitle: Text(product.description),
        );
      },
    );
  }
}
