import 'package:e_commerce/presentation/pages/home/dashboard/dashboard.controller.dart';
import 'package:flutter/material.dart';

import '../../home/dashboard/dashboard.widget.dart';

class LoginController {
  final dashboardController = DashboardController();
  final formKey = GlobalKey<FormState>();
  final TextEditingController userNameController = TextEditingController();

  void login(BuildContext context) {
    final userName = userNameController.text;

    if (formKey.currentState?.validate() ?? false) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Dashboard(
            dashboardController: dashboardController,
            userName: userName,
          ),
        ),
      );
    }
  }

  String? textFormFieldValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Este campo é obrigatório.';
    }
    return null;
  }
}
