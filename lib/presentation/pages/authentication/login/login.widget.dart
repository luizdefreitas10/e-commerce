import 'package:flutter/material.dart';
import 'login.controller.dart';

class LoginWidget extends StatelessWidget {
  const LoginWidget({required this.controller, super.key});

  final LoginController controller;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          horizontal: 100,
          vertical: 50,
        ),
        child: Column(
          children: [
            Form(
              key: controller.formKey,
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: size.width,
                ),
                child: TextFormField(
                  validator: (value) => controller.textFormFieldValidator(value),
                  decoration: const InputDecoration(
                    label: Text('Nome'),
                  ),
                  controller: controller.userNameController,
                ),
              ),
            )
          ],
        ),
      ),
      persistentFooterAlignment: AlignmentDirectional.bottomCenter,
      persistentFooterButtons: [
        TextButton(
          onPressed: () => controller.login(context),
          child: const Text('Entrar'),
        ),
      ],
    );
  }
}
