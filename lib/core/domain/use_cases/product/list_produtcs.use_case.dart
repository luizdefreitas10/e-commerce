import 'package:e_commerce/core/domain/models/product_model.dart';
import 'package:e_commerce/infrastructure/repositories/product.repository.dart';

class ListProductUseCase {
  final repository = ProductRepository();

  Future<List<Product>> call() {
    return repository.listProducts();
  }
}
