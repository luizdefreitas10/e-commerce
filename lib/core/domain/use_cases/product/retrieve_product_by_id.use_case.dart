import '../../../../infrastructure/repositories/product.repository.dart';
import '../../models/product_model.dart';

class RetrieveProductByIdUseCase {
  final ProductRepository repository = ProductRepository();

  Future<Product?> call(String id) {
    return repository.getProductById(id);
  }
}
