import 'package:e_commerce/core/domain/models/cart_item.dart';

class SalesRecord {
  SalesRecord({
    required this.id,
    required this.customerName,
    required this.recordedAt,
    required this.cartItems,
    required this.totalPrice,
  });

  final List<CartItem> cartItems;
  final String customerName;
  final String id;
  final DateTime recordedAt;
  final String totalPrice;
}
