import 'package:e_commerce/core/domain/models/product_model.dart';

class CartItem {
  CartItem({
    required this.id,
    required this.product,
    required this.quantity,
    required this.unitPrice,
    required this.totalPrice,
  });

  final String id;
  final Product product;
  final int quantity;
  final String totalPrice;
  final String unitPrice;
}
